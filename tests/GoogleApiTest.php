<?php
declare(strict_types=1);

namespace Nora\GoogleApi;

use Nora\Kvs\StorageContext;
use Nora\Kvs\StorageFactory;
use PHPUnit\Framework\TestCase;


class GoogleApiTest extends TestCase
{
    /**
     * @test
     */
    public function 設定のテスト()
    {
        $context = new GoogleApiContext([
            'name' => 'test',
            'authConfig' => __DIR__.'/resource/oauth-secret.json',
            'scopes' => [
                'https://www.googleapis.com/auth/plus.login'
            ],
            'storage' => [
                'type' => 'FileSystem',
                'name' => '/tmp'
            ]
        ]);

        $this->assertEquals('test', $context->name);
        $this->assertEquals('FileSystem', $context->storage->type);
        return $context;
    }

    /**
     * @test
     * @depends 設定のテスト
     */
    public function クライアントのテスト(GoogleApiContext $context)
    {
        $client = new GoogleApiClient($context);
        var_dump($client->getAccessToken());
        echo $url;
    }
}
