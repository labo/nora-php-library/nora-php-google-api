<?php
namespace Nora\GoogleApi\Authentication;

use Nora\GoogleApi\GoogleApiClient;

class WaitAuthCodeStdin implements WaitAuthCodeInterface
{
    public function __invoke(string $url)
    {
        printf("Open: \n%s\n", $url);
        flush();
        if (ob_get_level() > 0) {
            ob_flush();
        }
        $auth_code = trim(fgets(STDIN));
        return $auth_code;
    }
}
