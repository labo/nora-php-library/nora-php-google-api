<?php
namespace Nora\GoogleApi\Authentication;

use Nora\GoogleApi\GoogleApiClient;

class OAuth
{
    private $client;

    public function __construct(GoogleApiClient $client, WaitAuthCodeInterface $waitAuthCode)
    {
        $this->client = $client;
        $this->waitAuthCode = $waitAuthCode;
    }

    public function getAuthUrl(string $callback_url = null)
    {
        $this->setup($callback_url);
        return $this->client->createAuthUrl();
    }

    public function setup($callback_url)
    {
        $this->client->setScopes(implode(',', $this->client->context->scopes));
        $this->client->setAccessType('offline');
        $this->client->setApprovalPrompt('force');
        if ($callback_url) {
            $this->client->setRedirectUri($callback_url);
        }
    }

    public function runAuthentication(string $code = null, string $callback_url = null)
    {
        $this->setup($callback_url);
        $access_token = $this->client->fetchAccessTokenWithAuthCode(
            $code // ?? ($this->waitAuthCode)($url)
        );
        // AccessTokenを保存する
        $this->client->saveAccessToken($access_token);
        return $access_token;
    }

    public function isAuthenticated() : bool
    {
        $access_token = $this->client->getAccessToken();
        if (empty($access_token)) {
            return false;
        }
        return $this->client->isAccessTokenExpired() ? false: true;
    }
}
