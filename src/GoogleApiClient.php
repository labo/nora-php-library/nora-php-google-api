<?php
namespace Nora\GoogleApi;

use Google_Client;
use Nora\GoogleApi\Authentication\OAuth;
use Nora\GoogleApi\Authentication\WaitAuthCodeStdin;
use Nora\Kvs\StorageFactory;

class GoogleApiClient extends Google_Client
{
    public $context;
    public OAuth $authentication;
    private $store;

    public static function create(array $options)
    {
        return new static(new GoogleApiContext($options));
    }

    public function __construct(GoogleApiContext $context)
    {
        $this->context = $context;
        $this->authentication = new OAuth($this, new WaitAuthCodeStdin);
        $this->store = StorageFactory::create($this->context->storage);
        parent::__construct();
        $this->setupClient();
    }

    protected function setupClient()
    {
        $this->setHttpClient(new \GuzzleHttp\Client([
            'headers' => [
                'X-GOOGLE-API-FORMAT-VERSION' => 2
            ]
        ]));
        $this->setApplicationName($this->context->applicationName);
        try {
            $this->setAuthConfig($this->context->authConfig);
        } catch (\Exception $e) {
            $config = $this->context->authConfig;
            throw new \LogicException("Invalid Json: ".$config);
        }

        $data = $this->getClientData();

        if (isset($data['access_token'])) {
            $this->setAccessToken($data['access_token']);
            // Reflesh対応
            if ($this->isAccessTokenExpired()) {
                $this->fetchAccessTokenWithRefreshToken($this->getRefreshToken());
                $this->saveAccessToken($this->getAccessToken());
            }
        }
    }

    protected function getClientData()
    {
        if ($this->store->hasItem($this->context->name)) {
            return $this->store->getItem($this->context->name);
        }
        return [];
    }

    protected function saveClientData($data)
    {
        return $this->store->saveItem($this->context->name, $data);
    }

    public function saveAccessToken($access_token)
    {
        $data = $this->getClientData();
        $data['access_token'] = $access_token;
        $this->saveClientData($data);
    }

}
