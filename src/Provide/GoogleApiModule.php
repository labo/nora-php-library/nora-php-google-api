<?php
namespace Nora\GoogleApi\Provide;

use Nora\Architecture\DI\Configuration\AbstractConfigurator;
use Google_Client;

class GoogleApiModule extends AbstractConfigurator
{
    public function configure()
    {
        $this->bind(Google_Client::class)
            ->toProvider(GoogleApiClientProvider::class);
    }
}
