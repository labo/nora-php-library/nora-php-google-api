<?php
namespace Nora\GoogleApi\Provide;

use Nora\Architecture\DI\Dependency\ProviderInterface;
use Nora\GoogleApi\GoogleApiClient;
use Nora\GoogleApi\GoogleApiContext;

class GoogleApiClientProvider implements ProviderInterface
{
    private $context;

    public function __construct(
        GoogleApiContext $context
    ) {
        $this->context = $context;
    }

    public function get()
    {
        return new GoogleApiClient($this->context);
    }
}
