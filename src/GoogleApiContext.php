<?php
namespace Nora\GoogleApi;

use Nora\DataStructure\StructedObject;
use Nora\DataStructure\Schema;
use Nora\Kvs\StorageContext;

class GoogleApiContext extends StructedObject
{
    public static function schema(Schema $params)
    {
        $params->name = Schema::string([
            'required' => true
        ]);

        $params->scopes = Schema::array(Schema::string());

        $params->storage = StorageContext::getSchema();

        $params->authConfig = Schema::string();

        $params->applicationName = Schema::string([
            'default' => 'NoraGoogleClient'
        ]);
    }
}
